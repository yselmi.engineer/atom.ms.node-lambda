const httpStatusesCodes = require("http-status-codes");

module.exports = (req, res) => {
  res.sendStatus(httpStatusesCodes.OK);
};
