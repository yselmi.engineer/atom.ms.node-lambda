const { MongoClient } = require("mongodb");
const _ = require("lodash");

const { DB_URL_TEMPLATE, DB } = require("../config");

let pool = null;

_.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

exports.connect = async (db) => {
  if (pool && pool.isConnected()) {
    return pool;
  }

  db =  db || DB;
  dbHost = process.env.DB_HOST || "127.0.0.1";

  const dbUrl = _.template(DB_URL_TEMPLATE)({ db, dbHost });

  const client = await MongoClient.connect(dbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  pool = client;

  return client;
};

exports.close = async () => pool.close();

exports.get = async (db) => {
  const client = await exports.connect(db);
  return client.db();
};
