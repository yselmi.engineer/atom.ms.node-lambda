
const { createCanvas, loadImage } = require('canvas');

const { MAX_TITLE_SIZE } = require('../../constant');

exports.generateJobImage = async (req, res, next) => {

    const { company, title, salaryMin, salaryMax } = req.data;

    const salaryMinView = `Appartir de ${salaryMin} DT/Mois !`;
   // const salaryMaxView = `Jusqu'a ${salaryMax} DT/Mois !`;

    // Define the canvas
    const width = 1080 // width of the image
    const height = 1080 // height of the image
    const canvas = createCanvas(width, height)
    const context = canvas.getContext('2d')

    // Define the font style
    context.textAlign = 'center'
    context.textBaseline = 'top'
    context.fillStyle = '#FFFFFF'
    // context.font = "80px 'signpainter' bold";

    const image = await loadImage('./src/images/job.jpg');

    // Draw the background
    context.drawImage(image, 0, 0, 1080, 1080)

    // Draw the text
    context.font = "50px 'Work Sans' bold";

    if (title.length > MAX_TITLE_SIZE) {
        context.fillText(`${title.substring(0, MAX_TITLE_SIZE - 1)}-`, 540, 50)
        context.fillText(title.substring(MAX_TITLE_SIZE-1, 200), 540, 120)
    }

    else {
        context.fillText(title, 540, 50)
    }

    context.font = "45px 'Work Sans' bold";
    context.fillText(salaryMinView, 540, 620);
   // context.fillText(salaryMaxView, 540, 690);


    context.font = "40px 'Work Sans' bold";
    context.fillStyle = '#FFFFFF'
    context.fillText(company, 540, 980)
    context.fillRect()

    // Convert the Canvas to a buffer
    const buffer = canvas.toBuffer('image/png')

    // Set and send the response as a PNG
    res.set({ 'Content-Type': 'image/png' });
    return res.send(buffer)
}