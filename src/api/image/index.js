const express = require("express");

const query = require("../../middleware/query");
const service = require("./service");

module.exports = express
  .Router()
  .get(
    "/generate/job",
    query.getFromQueryString,
    service.generateJobImage
  )
  .post(
    "/generate/job",
    query.getFromPost,
    service.generateJobImage
  )
