const { ObjectId } = require("mongodb");
const httpStatusCodes = require("http-status-codes");
const serialize = require("../services/serializer");

module.exports = {
  serializeCreate: async (req, res) => {
    res.status(httpStatusCodes.CREATED).json(
      await serialize(req, {
        Model: req.render.Model,
        options: {
          group: req.serializeGroup || "details",
          refs: req.render._id,
        },
      })
    );
  },
  serializeOKFromRouterId: async (req, res) => {
    res.status(httpStatusCodes.OK).json(
      await serialize(req, {
        Model: req.render.Model,
        options: {
          group: req.serializeGroup || "details",
          refs: ObjectId(req.params._id),
        },
      })
    );
  },
  serializeGet: async (req, res) => {
    res.status(httpStatusCodes.OK).json(
      await serialize(req, {
        Model: req.render.Model,
        options: { group: "details", refs: req.render._id },
      })
    );
  },
  serializeOk: async (req, res, next) => {
    res.status(httpStatusCodes.OK).json(
      await serialize(req, {
        Model: req.render.Model,
        options: { group: req.serializeGroup, refs: req.render._id },
      })
    );
  },
  create: (req, res) => {
    res.status(httpStatusCodes.CREATED).json(req.render);
  },
  get: (req, res) => {
    res.status(httpStatusCodes.OK).json(req.render);
  },
  ok: (req, res) => {
    res.status(httpStatusCodes.OK).json(req.render);
  },
  delete: (req, res) => {
    res.sendStatus(httpStatusCodes.NO_CONTENT);
  },
};
