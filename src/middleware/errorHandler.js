const pino = require("pino");
const config = require("../config");

const logger = pino({ level: config.LOG_LEVEL });

module.exports = (err, req, res, next) => {
  logger.error(err);

  let message = "";
  if (res.statusCode !== 500) {
    message = err.message;
  } else if (process.env.NODE_ENV !== "production") {
    message = err.message;
  } else {
    message = "An error has occured while processing your operation";
  }

  res.status(err.statusCode || 500).send({ message });
  next();
};
